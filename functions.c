#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "functions.h"



float pythagore(Coordonnees_Points *tableau)
{
    float resultat = 0;
    int i=0;
    for(i=0; i<6; i++){
        resultat = resultat + sqrt(pow(tableau[i+1].x-tableau[i].x,2)+pow(tableau[i+1].y-tableau[i].y,2));

    }
    return resultat;
}
